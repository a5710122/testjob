// load module
const express = require('express');

// PORT
const port = 8000;

// Call API
const route_api = require('./api')

// Create Instance express
const app = express()

// Call CORS
app.use((req, res, next) => {
  var header = { 'Access-Control-Allow-Origin': '*' }
  for (var i in req.headers) {
    if (i.toLowerCase().substr(0, 15) === 'access-control-') {
      header[i.replace(/-request-/g, '-allow-')] = req.headers[i]
    }
  }
  res.header(header)
  next()
})


// Use API
app.use('/vendor', route_api)

app.get('/', (req, res) => {
  try {
    return res.status(200).send("Hello");
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'ไม่สามารถเชื่อมต่อได้'
    });
  }
})


// PORT
app.listen(port, () => {
  console.log(`Start server at port ${port}.`)
})
