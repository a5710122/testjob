const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
module.exports = router

const mysql = require('mysql');

router.use(bodyParser.json())
router.use(
  bodyParser.urlencoded({
    extended: true
  })
)


//Connected MySQL
var connection = mysql.createConnection({
  host : 'localhost',
  user : 'root',
  password : '',
  database : 'mytestapp'
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

router.get('/', (req, res) => {
  try {
    var sql = "SELECT ven_code, pre_name, add_tel, contact_name, status FROM sb_vendor"
    connection.query(sql, function(err, result){
      if (err) throw err
      res.json(result)
    });
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'ไม่สามารถเชื่อมต่อได้'
    });
  }
})

router.get('/:search',(req,res) => {
  try {
    const { search } = req.params
    var sql = "SELECT * FROM sb_vendor WHERE ven_code = ? OR pre_name = ? OR add_tel = ?"
    connection.query(sql, [search, search, search], function(err, result){
      if (err) throw err
      res.json(result)
    });
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'ไม่สามารถเชื่อมต่อได้'
    });
  }
})


// app.get('/add',(req,res) => {
// 	res.render('add');
// });

router.post('/add',(req,res) => {

  try {
    const branch_no = req.body.branch_no;
    const ven_code = req.body.ven_code
    const add_tel = req.body.add_tel;
    const ven_name = req.body.ven_name;
    const contact_name = req.body.contact_name;

    const post = {
      branch_no:branch_no,
      ven_code:ven_code,
      add_tel:add_tel,
      ven_name:ven_name,
      contact_name:contact_name
    }

    var sql = "INSERT INTO sb_vendor SET ?"
    connection.query(sql, [post], function(err, result){
      if (err) throw err
      //res.json(result)
      return res.status(200).send({
        status: 200,
        message: 'บันทึกข้อมูล'
      });
    });
  }catch (error) {

    return res.status(500).send({
      status: 500,
      message: 'ไม่สามารถเชื่อมต่อได้'
    });
  }
})

// app.get('/edit/:id',(req,res) => {
//
// 	const edit_postID = req.params.id;
//
// 	connection.query('SELECT * FROM posts WHERE id=?',[edit_postID],(err,results) => {
// 		if(results){
// 			res.render('edit',{
// 				post:results[0]
// 			});
// 		}
// 	});
// });
//
// app.post('/edit/:id',(req,res) => {
// 	const update_title = req.body.title;
// 	const update_content = req.body.content;
// 	const update_author_name = req.body.author_name;
// 	const userId = req.params.id;
// 	connection.query('UPDATE `posts` SET title = ?, content = ?, author = ? WHERE id = ?', [update_title, update_content, update_author_name, userId], (err, results) => {
//         if(results.changedRows === 1){
//             console.log('Post Updated');
//         }
// 		return res.redirect('/');
//     });
// });
//
// app.get('/delete/:id',(req,res) => {
//     connection.query('DELETE FROM `posts` WHERE id = ?', [req.params.id], (err, results) => {
//         return res.redirect('/');
//     });
// });
